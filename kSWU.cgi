#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/bin/perl

# ------------------------------------------------------------------------------------
# filename: kSWU.pl
# author:   Frode Klevstul (frode@klevstul.com) (http://klevstul.com)
# started:  16.12.2004
# ------------------------------------------------------------------------------------
#
# Revision - Newest version at the top - - - - - - - - - - - - - - - - - - - - - - - -
# version:  v02_20050624
#           - Minor layout changes.
# version: v02_20050517
#          - Introduced "BLOCKAREA" and "BLOCK" tags for archiving and content
#            rearrangement purposes.
#          - Layout changes.
#          - Substitutes URLs with links in textareas.
#          - Don't show mode switch unless in preview.
#          - Fixed LINK:URL error that occured in simple mode.
#          - Fixed newline bug inside <pre></pre> caused by \r (return) in DOS.
# version: v01_20041216
#          - The first version of this program was made.
# ------------------------------------------------------------------------------------

# ------------------------------------------------------------------------------------
# Legal kSWU tags
# ------------------------------------------------------------------------------------
#
# A BLOCKAREA consists of several blocks.
# <o--kSWU:BLOCKAREA--o> <o--/kSWU:BLOCKAREA--o>
#
# Each BLOCK in a blockarea can be moved around as individual elements.
# <o--kSWU:901:BLOCK--o> <o--/kSWU:901--o>
#
#
# Type I: File|Img
# <o--kSWU:001:FILE:IMG--o> <o--/kSWU:001--o>
#
# Type II: Link|File
# <o--kSWU:002:LINK:FILE--o> <o--/kSWU:002--o>
#
# Type III: Link|URL
# <o--kSWU:003:LINK:URL--o> <o--/kSWU:003--o>
#
# Type IV: Text|Text
# <o--kSWU:004:TEXT:TEXT--o> <o--/kSWU:004--o>
#
# Type V: Text|Textarea
# <o--kSWU:005:TEXT:TEXTAREA--o> <o--/kSWU:005--o>
# ------------------------------------------------------------------------------------



# -------------------
# use
# -------------------
use strict;
use File::Copy;
use klevstul;
&kStartPage("HTML");																				# print the code to start an HTML page



# -------------------
# declarations
# -------------------
my $version 				= "v02_20050624";
my $path 					= &kGetPath;															# retrieve the path to where this program is running
my $iniFile					= $path . "kSWU.ini";													# the file containing all configuration values
my @html_top				= &kGetIniValue($iniFile, "HTML_TOP");
my @html_bottom				= &kGetIniValue($iniFile, "HTML_BOTTOM");
my @file					= &kGetIniValue($iniFile, "FILE");
my @urlbase					= &kGetIniValue($iniFile, "URLBASE");
my @bottom_root				= &kGetIniValue($iniFile, "BOTTOM_ROOT");								# the path to the root of the webserver this script runs
my $scriptURL 				= "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}";						# the url to this scrip on the webserver
my $scriptbase				= $scriptURL;
my $operation				= &kQuery('op');														# what operation to execute
my $file					= &kQuery('file');														# what file to work on
my $base					= &kQuery('base');
my $mode					= &kQuery('mode');
my $parameters				= &kQuery('parameters');
$scriptbase					=~ s/(.*)[\/\\].*/$1/;



# -------------------
# main program
# -------------------
&kPrintHtmlPage($html_top[0]);																		# print the top HTML code
&kSWU_printMenu;																					# print kSWU menu links
if ($operation eq "choose"){
	&kSWU_chooseFiles;
} elsif ($operation eq "display"){
	&kSWU_displayFile;
} elsif ($operation =~ m/^update/){																	# "update" (preview only) or "update_action" (write changes to file)
	&kSWU_updateFile;
} elsif ($operation =~ m/switch_mode-(.*)/) {
	$operation = $1;
	if ($mode eq "simple"){
		$mode = "advanced";
	} elsif ($mode eq "advanced"){
		$mode = "simple";
	}
	&kJumpTo($scriptURL."?mode=".$mode."&op=".$operation."&file=".$file."&base=".$base);
} elsif ($operation =~ m/block-(\d{3})-(.*)/) {														# all block commands
	# operation=block-901-de_activate
	&kSWU_blockOperation($1,$2);
}
&kPrintHtmlPage($html_bottom[0]);																	# print the bottom HTML code



# -------------------
# sub
# -------------------
sub kSWU_printMenu{
	my $extra;

	if ($file ne "" && $operation eq "display"){
		$extra = " '$file' | <a href=\"$scriptURL?op=switch_mode-$operation&mode=$mode&file=$file&base=$base\"><img border=\"0\" src=\"$scriptbase/icons/$mode.gif\"></a> | <a href=\"javascript:document.form_update.submit()\"><img border=\"0\" src=\"$scriptbase/icons/preview.gif\"></a>";
	} elsif ($file ne "" && $operation =~ m/update/){
		$extra = " '$file' | <a href=\"javascript:document.form_update.submit()\"><img border=\"0\" src=\"$scriptbase/icons/save.gif\"></a> | <a href=\"javascript:history.go(-1)\"><img border=\"0\" src=\"$scriptbase/icons/goBack.gif\"></a>";
	}

	if ($mode eq ""){
		$mode = "simple";
	}

	print qq(<center><a style="text-decoration: none" href="http://klevstul.com/kSWU" target="_blank">kSWU <i>$version</i></a></center><br>);
	print qq(<img src="$scriptbase/icons/kSWU_logo.gif"> | <a href="$scriptURL?op=choose&mode=$mode"><img border=\"0\" src=\"$scriptbase/icons/setFile.gif\"></a>$extra |<br><hr noshade size="1"><br>\n);
}



sub kSWU_chooseFiles{
	my $i;

	foreach (@file){
		print qq(<a href="$scriptURL?op=display&file=$_&base=$urlbase[$i]&mode=$mode">$_</a><br>\n);
		$i++;
	}
}



sub kSWU_displayFile{
	my $line;																						# line of text read from file
	my $file_content;																				# content of the entire file
	my @kSWU_tags;																					# array of file content, split on kSWU tags
	my $kSWU_tag;																					# for use in foreach @kSWU_tag loop
	my $id;																							# the ID of the kSWU tag
	my $src;																						# the source of images (IMG SRC="")
	my $src_filename;																				# the path stripped away from $src, filname only
	my $href;																						# the href in A HREF=""
	my $href_filename;																				# the filename with the path stripped away if HREF to a file
	my $target;																						# the target of a A HREF TARGET="" link
	my $link_name;																					# the name of a link (<a href>link name</a>)
	my $text;																						# the text in a text area of input text field
	my $dir_src;																					# the directory of a src (IMG SRC="") path, with the filename stripped away
	my $dir_file;																					# the directory to the file $file
	my $dir_href;																					# the directory to the href is href is to a file
	my @files;																						# all files in a given directory
	my $this_file;																					# for use in foreach @files loop
	my $size;																						# lenght for use in size of input fields
	my $size2;																						# lenght for use in size of input fields
	my $other_content;																				# other content, outside of kSWU tags
	my $tmp;

	# ----------------------------------------------------------
	# Create a preview copy (filename.html.preview) of the
	# original file.
	# ----------------------------------------------------------
	if (-e $file && $file !~ m/\.preview/s){
		copy($file, $file.".preview") || &kError("kSWU_displayFile", "failed to copy '$file'");
		$file .= ".preview";
	}

	# ----------------------------------------------------------
	# Open the preview file, read out all the content, manipulate
	# content needed to be manipulated and write it back.
	# It might seem a bit akward, but is needed since the file
	# content is read from file again in other procedures, and
	# not passed on as a argument.
	# ----------------------------------------------------------
	open(FILE, "<$file") || &kError("kSWU_displayFile", "failed to open '$file'");
	LINE: while ($line = <FILE>){
		$file_content .= $line;
	}
	close (FILE);

	# substitutes HTML comments with kSWU deactivation blocks for previewing
	$file_content =~ s/<!--kSWU:DEACTIVATED/<o--kSWU:DEACTIVATED--o>/sg;
	$file_content =~ s/\/kSWU:DEACTIVATED-->/<o--\/kSWU:DEACTIVATED--o>/sg;

	# write content back to the preview file
	open(FILE, ">$file") || &kError("kSWU_displayFile", "failed to open '$file'");
	print FILE $file_content;
	close (FILE);

	# ----------------------------------------------------------
	# Start parsing the file content.
	# ----------------------------------------------------------
	$file_content =~ s/<o--kSWU:/���<o--kSWU:/sg;													# add '���' for splitting

	if ($mode eq "advanced"){
		$file_content =~ s/<o--kSWU:BLOCKAREA--o>/<b>[BLOCKAREA START]<\/b>/s;						# replace BLOCKAREA tags...
		$file_content =~ s/<o--\/kSWU:BLOCKAREA--o>/<b>[BLOCKAREA STOP]<\/b>/s;						# and show in browser if advanced mode
	}

	@kSWU_tags = split('���', $file_content);														# split on each kSWU start tag

	# ---
	# replace kSWU tags with input forms etc...
	# ---
	foreach $kSWU_tag (@kSWU_tags){
		# ---
		# kSWU : BLOCK
		#
		# <o--kSWU:901:BLOCK--o>Block content<o--/kSWU:901--o>
		# ---
		if ($kSWU_tag =~ m/^<o--kSWU:(\d{3}):BLOCK--o>/s){
			$id		= $1;
			$tmp	= $file;
			$tmp	=~ s/\\/\\\\/sg;																# replace '\' with '\\' so the jacascript funtion won't remove the '\' character in systems paths
			$href = qq(
				<script language="JavaScript">
				<!--
					function confirm_delete(id,url){
						input_box = confirm("Are you sure you want to delete block #"+id+"?");
						if (input_box==true){
							window.location.href = url;
						}
					}
				-->
				</script>
			
				<nobr><b>[BLOCK #$id: |
				<a href="$scriptURL?op=block-$id-up&file=$file&base=$base&mode=$mode">Up</a> |
				<a href="$scriptURL?op=block-$id-down&file=$file&base=$base&mode=$mode">Down</a> |
				<a href="$scriptURL?op=block-$id-top&file=$file&base=$base&mode=$mode">Top</a> |
				<a href="$scriptURL?op=block-$id-bottom&file=$file&base=$base&mode=$mode">Bottom</a> |
				<a href="javascript:confirm_delete('$id','$scriptURL?op=block-$id-delete&file=$tmp&base=$base&mode=$mode')">Delete</a> |
				<a href="$scriptURL?op=block-$id-duplicate&file=$file&base=$base&mode=$mode">Duplicate</a> |
				<a href="$scriptURL?op=block-$id-de_activate&file=$file&base=$base&mode=$mode">De-/activate</a> |
				]</b></nobr>
				<br>
			);

			if ($mode eq "advanced"){
				$kSWU_tag		=~ s/(<o--kSWU:$id:BLOCK--o>)/$1 $href/s;							# add block navigation
			}
		# ---
		# kSWU type: FILE:IMG
		#
		# <o--kSWU:001:FILE:IMG--o><img src="dice1.gif"><o--/kSWU:001--o>
		# ---
		} elsif ($kSWU_tag =~ m/^<o--kSWU:(\d{3}):FILE:IMG--o>/s){
			$id				= $1;
			$kSWU_tag		=~ m/^<o--kSWU:($id):FILE:IMG--o>\s*<img.*src="(.*)".*>\s*<o--\/kSWU:$id--o>(.*)$/s;
			$src			= $2;
			$other_content	= $3;
			$kSWU_tag		=~ s/^(<o--kSWU:$id:FILE:IMG--o>).*$/$1/s;
			$src_filename	= &kGetFilename($src);
			$dir_src		= &kGetDir($src);
			$dir_file		= &kGetDir($file);
			@files 			= &kListDirectory($dir_file."/".$dir_src, 1);
			$parameters		.= $id . "_FILE:IMG" . "-";

			$kSWU_tag .= qq(<select name="kSWU_$id">);

			foreach $this_file (@files){
				if ($this_file eq $src_filename){
					$kSWU_tag .= qq(<option selected>$dir_src/$this_file</option>);
				} else {
					$kSWU_tag .= qq(<option>$dir_src/$this_file</option>);
				}
			}
			$kSWU_tag .= qq(</select>);
			$kSWU_tag .= $other_content;
		# ---
		# kSWU type: LINK:FILE
		#
		# <o--kSWU:002:LINK:FILE--o><a href="img/dice1.gif" target="blank">link</a><o--/kSWU:002--o>
		# ---
		} elsif ($kSWU_tag =~ m/^<o--kSWU:(\d{3}):LINK:FILE--o>/s){
			$id				= $1;
			$kSWU_tag		=~ m/^<o--kSWU:($id):LINK:FILE--o>\s*<a.*href="(.*)"\s*target="(.*)".*>(.*)<\/a>\s*<o--\/kSWU:$id--o>(.*)$/s;
			$href			= $2;
			$target			= $3;
			$link_name		= $4;
			$other_content	= $5;
			$size			= length($link_name);
			$kSWU_tag		=~ s/^(<o--kSWU:$id:LINK:FILE--o>).*/$1/s;
			$href_filename 	= &kGetFilename($href);
			$dir_href		= &kGetDir($href);
			$dir_file		= &kGetDir($file);
			@files			= &kListDirectory($dir_file."/".$dir_href, 1);
			$parameters		.= $id . "_LINK:FILE" . "-";

			$kSWU_tag .= qq(<select name="kSWU_$id">);
			foreach $this_file (@files){
				if ($this_file eq $href_filename){
					$kSWU_tag .= qq(<option selected>$dir_href/$this_file</option>);
				} else {
					$kSWU_tag .= qq(<option>$dir_href/$this_file</option>);
				}
			}
			$kSWU_tag .= qq(</select>);

			if ($mode eq "advanced"){
				$kSWU_tag .= qq(&nbsp;name: <input size="$size" type="text" name="kSWU_$id\_linkname" value="$link_name">);
				$kSWU_tag .= qq(&nbsp;target: <input size="6" type="text" name="kSWU_$id\_target" value="$target">);
			} elsif ($mode eq "simple"){
				$kSWU_tag .= qq(<input type="hidden" name="kSWU_$id\_linkname" value="$link_name">);
				$kSWU_tag .= qq(<input type="hidden" name="kSWU_$id\_target" value="$target">);
			}
			$kSWU_tag .= $other_content;
		# ---
		# kSWU type: LINK:URL
		#
		# <o--kSWU:003:LINK:URL--o><a href="http://klevstul.com" target="_blank">klevstul.com</a><o--/kSWU:003--o>
		# ---
		} elsif ($kSWU_tag =~ m/^<o--kSWU:(\d{3}):LINK:URL--o>/s){
			$id				= $1;
			$kSWU_tag 		=~ m/^<o--kSWU:($id):LINK:URL--o>\s*<a.*href="(.*)"\s*target="(.*)".*>(.*)<\/a>\s*<o--\/kSWU:$id--o>(.*)$/s;
			$href			= $2;
			$target			= $3;
			$link_name		= $4;
			$other_content	= $5;
			$size			= length($href);
			$size2			= length($link_name);
			$kSWU_tag 		=~ s/^(<o--kSWU:$id:LINK:URL--o>).*/$1/s;
			$parameters		.= $id . "_LINK:URL" . "-";

			$kSWU_tag .= qq(<input size="$size" type="text" name="kSWU_$id" value="$href">);
			if ($mode eq "advanced"){
				$kSWU_tag .= qq(&nbsp;name: <input size="$size2" type="text" name="kSWU_$id\_linkname" value="$link_name">);
				$kSWU_tag .= qq(&nbsp;target: <input size="6" type="text" name="kSWU_$id\_target" value="$target">);
			} elsif ($mode eq "simple"){
				$kSWU_tag .= qq(<input type="hidden" name="kSWU_$id\_linkname" value="$link_name">);
				$kSWU_tag .= qq(<input type="hidden" name="kSWU_$id\_target" value="$target">);
			}
			$kSWU_tag .= $other_content;
		# ---
		# kSWU type: TEXT:TEXT
		#
		# <o--kSWU:004:TEXT:TEXT--o>Here is the text<o--/kSWU:004--o>
		# ---
		} elsif ($kSWU_tag =~ m/^<o--kSWU:(\d{3}):TEXT:TEXT--o>/s){
			$id				= $1;
			$kSWU_tag 		=~ m/^<o--kSWU:($id):TEXT:TEXT--o>(.*)<o--\/kSWU:$id--o>(.*)$/s;
			$text			= $2;
			$other_content	= $3;
			$size			= length($text);
			$kSWU_tag 		=~ s/^(<o--kSWU:$id:TEXT:TEXT--o>).*/$1/s;
			$parameters		.= $id . "_TEXT:TEXT" . "-";
			$text 			= &kHTMLify($text);

			$kSWU_tag .= qq(<input size="$size" type="text" name="kSWU_$id" value="$text">);
			$kSWU_tag .= $other_content;
		# ---
		# kSWU type: TEXT:TEXTAREA
		#
		# <o--kSWU:005:TEXT:TEXTAREA--o>And here is more text, yeah actually much more text...<o--/kSWU:005--o>
		# ---
		} elsif ($kSWU_tag =~ m/^<o--kSWU:(\d{3}):TEXT:TEXTAREA--o>/s){
			$id				= $1;
			$kSWU_tag 		=~ m/^<o--kSWU:($id):TEXT:TEXTAREA--o>(.*)<o--\/kSWU:$id--o>(.*)$/s;
			$text			= $2;
			$other_content	= $3;
			$kSWU_tag 		=~ s/^(<o--kSWU:$id:TEXT:TEXTAREA--o>).*/$1/s;
			$parameters		.= $id . "_TEXT:TEXTAREA" . "-";
			$text			= &kStripLinks($text);													# strip all links for HTML code
			$text 			= &kHTMLify($text);														# make existing HTML viewable in a form

			$kSWU_tag .= qq(<textarea rows="8" cols="80" name="kSWU_$id">$text</textarea>);
			$kSWU_tag .= $other_content;
		}
	}

	# ---
	# print HTML code
	# ---
	print qq(
		<base href= "$base">
		<form name ="form_update" action="$scriptURL" method="post">
		<input type="hidden" name="op" value="update">
		<input type="hidden" name="base" value="$base">
		<input type="hidden" name="file" value="$file">
		<input type="hidden" name="mode" value="$mode">
		<input type="hidden" name="parameters" value="$parameters">
	);
	foreach (@kSWU_tags){
		print $_;
	};
	print qq(
		</form>
		<base href= "$bottom_root[0]">
	);
}



sub kSWU_updateFile{
	my @params = split('-', $parameters);															# parameters split by '-'
	my %value;																						# the parameters values to be stored in this parameter
	my $line;
	my $file_content;
	my $param_id;
	my %param_type;
	my $i;
	my $tmp;

	# ----------------------------------------------------------
	# Go through all parameters sent to this procedure
	# and assign values to these parameters.
	#
	# Examples:
	# $parameters:
	#	<input type="hidden" name="parameters" value="001_FILE:IMG-011_FILE:IMG-002_LINK:FILE-">
	# Names of elements in a form:
	#	<select name="kSWU_001">
	#	<select name="kSWU_011">
	#	<select name="kSWU_002">
	#
	# This way we can create a hash of all parameters, and their values:
	# $value{$param_id} = &kQuery("kSWU_".$param_id);
	# $value{001} = &kQuery("kSWU_001");
	# ----------------------------------------------------------
	foreach (@params){																				# Assigning values to parameters
		$param_id				= $_;																# $param is on the form "001_FILE:IMG"
		$tmp					= $_;
		$param_id				=~ s/(\d{3})_(.*)/$1/;												# first part is parameter name (001)
		$tmp					=~ s/(\d{3})_(.*)/$2/;												# second part is parameter type (FILE:IMG)
		$param_type{$param_id} 	= $tmp;
		$value{$param_id} 		= &kQuery("kSWU_".$param_id);										# $value{'kSWU_001'} = 'Here is the text'
		$value{$param_id}		= kHTMLify($value{$param_id});										# make HTML code out of characters like '<', '>' etc...

		if ($param_type{$param_id} =~ m/LINK/){
			$value{$param_id."_target"}= &kQuery("kSWU_".$param_id."_target");						# if it's of type link a target is sent
			$value{$param_id."_linkname"}= &kQuery("kSWU_".$param_id."_linkname");					# as well as the name of the link
			if ( $value{$param_id."_linkname"} eq "" ){												# in simple-mode linkname and target is not sent / given
				$value{$param_id."_linkname"} = $value{$param_id};									# so we use url as linkname
				$value{$param_id."_target"} = "_blank";												# and "_blank" as target
			}
		}
		$i++;
	}

	# ----------------------------------------------------------
	# Read out and store all the content in the preview file.
	# ----------------------------------------------------------
	open(FILE, "<$file") || &kError("kSWU_updateFile", "failed to open '$file'");
	LINE: while ($line = <FILE>){
		$file_content .= $line;
	}
	close (FILE);

	# ----------------------------------------------------------
	# These new values has not been written to file yet, so
	# we go through the content of the file, and replace the
	# old values with these new values.
	# ----------------------------------------------------------
	foreach (sort keys %value){
		if ($param_type{$_} eq "FILE:IMG"){
			$file_content =~ s/(<o--kSWU:$_:FILE:IMG--o>).*(<o--\/kSWU:$_--o>.*)/$1<img src="$value{$_}">$2/s;
		} elsif ($param_type{$_} =~ m/LINK:/){
			$file_content =~ s/(<o--kSWU:$_:LINK:.*--o>).*(<o--\/kSWU:$_--o>.*)/$1<a href="$value{$_}" target="$value{$_."_target"}">$value{$_."_linkname"}<\/a>$2/s;
		} elsif ($param_type{$_} =~ m/TEXT:/){
			$tmp = $value{$_};
			if ($param_type{$_} =~ m/TEXT:TEXTAREA/){
				$tmp			= &kMakeLinks($tmp);												# makes links out of URLs in textareas
			}
			$file_content	=~ s/(<o--kSWU:$_:TEXT:.*--o>).*(<o--\/kSWU:$_--o>.*)/$1$tmp$2/s;
		}
	}

	# ----------------------------------------------------------
	# Since we're using special deactivation tags when displaying a page
	# (the content that is deactivated is still viewable when in display mode)
	# we have to substitute these blocks with proper HTML comments so the
	# deactivated content won't be showed in preview mode
	# ----------------------------------------------------------
	$file_content =~ s/<o--kSWU:DEACTIVATED--o>/<!--kSWU:DEACTIVATED/sg;
	$file_content =~ s/<o--\/kSWU:DEACTIVATED--o>/\/kSWU:DEACTIVATED-->/sg;

	# ----------------------------------------------------------
	# If preview mode we open standard out and print all content
	# to the screen. If the users has decided to update the
	# content, we print the content back to the original file.
	# ----------------------------------------------------------
	if ($operation eq "update"){
		open (FILE, ">&STDOUT");																	# print to standard out
		print FILE qq(<base href= "$base">);														# preview only
	} elsif ($operation eq "update_action"){
		if (-e $file){
			$file =~ s/\.preview//s;
			move($file.".preview", $file) || &kError("kSWU_updateFile", "failed to move '$file'");	# move from "filename.html.preview" to "filename.html"
			open(FILE, ">$file") || &kError("kSWU_updateFile", "failed to open '$file'");			# write content back to the file
			$file_content =~ s/\r//sg;																# remove DOS Return characters
		} else {
			&kError("kSWU_updateFile", "failed to open '$file'");
		}
	}
	print FILE $file_content;
	close (FILE);

	# prints out HTML
	if ($operation eq "update"){
		print qq(
			<form name ="form_update" action="$scriptURL" method="post">
			<input type="hidden" name="op" value="update_action">
			<input type="hidden" name="base" value="$base">
			<input type="hidden" name="file" value="$file">
			<input type="hidden" name="mode" value="$mode">
			<input type="hidden" name="parameters" value="$parameters">
		);
		foreach (sort keys %value){
			print qq(
				<input type="hidden" name="kSWU_$_" value="$value{$_}">
			);
			if ($value{$_."_target"} ne ""){
				print qq(
					<input type="hidden" name="kSWU_$_\_target" value="$value{$_."_target"}">
					<input type="hidden" name="kSWU_$_\_linkname" value="$value{$_."_linkname"}">
				);
			}
		}
	} elsif ($operation eq "update_action"){
		&kJumpTo($scriptURL."?mode=".$mode."&op=choose");
	}

	print qq(<base href= "$bottom_root[0]">);
}



sub kSWU_blockOperation{
	my $blockNo		= $_[0];
	my $operation	= $_[1];

	my $line;
	my $file_content;
	my $pre_blockarea;
	my $post_blockarea;
	my @blocks;
	my $position;
	my @tmp;
	my $tmp;
	my @kSWUIDs;

	# ----------------------------------------------------------
	# Read from file.
	# ----------------------------------------------------------
	open(FILE, "<$file") || &kError("kSWU_blockOperation", "failed to open '$file'");
	LINE: while ($line = <FILE>){
		$file_content .= $line;
	}
	close (FILE);

	# ----------------------------------------------------------
	# To be able to give unique kSWU IDs to a duplicated block,
	# we have to go through all kSWU tags and get their IDs
	# ----------------------------------------------------------
	$tmp = $file_content;
	$tmp =~ s/(<o--kSWU)/���$1/sg;
	@tmp = split('���', $tmp);

	foreach (@tmp){
		if ($_ =~ m/<o--kSWU:(\d{3})/s){																	# find all block numbers / block IDs
			push(@kSWUIDs, $1);																		# and push them on the array
		}
	}

	# ----------------------------------------------------------
	# Split the content into:
	# - Pre Blockarea (everything before start BLOCKAREA tag)
	# -		Block 1
	# -		Block 2
	# -		Block 3 etc...
	# - Post Blockarea (everything after last BLOCKAREA tag)
	# ----------------------------------------------------------
	$file_content =~ s/(.*<o--kSWU:BLOCKAREA--o>)(.*)/$2/s;											# remove everything before BLOCKAREA tag
	$pre_blockarea = $1;

	$file_content =~ s/(.*)(<o--\/kSWU:BLOCKAREA--o>.*)/$1/s;										# remove everything after "end of BLOCKAREA" tag
	$post_blockarea = $2;

	$file_content =~ s/(<o--kSWU:\d{3}:BLOCK--o>)/���$1/sg;
	@blocks = split ('���', $file_content);
	shift @blocks;																					# remove first element of the array, since that is empty

	$position = 0;
	FOREACH: foreach (@blocks){
		if ($_ =~ m/<o--kSWU:$blockNo:BLOCK--o>/s){													# find the position of the block with ID/Block number $blockNo
			last FOREACH;
		}
		$position++;
	}

	# ----------------------------------------------------------
	# Perform the right operation ($operation) on the right
	# block with postion $position in the array containing
	# all the blocks (@blocks)
	# ----------------------------------------------------------

	# ---
	# Operation: up | down | top | bottom
	# ---
	if ($operation eq "up" || $operation eq "down" || $operation eq "top" || $operation eq "bottom"){
		@blocks = &kArrayOp($operation, $position, @blocks);										# kArrayOp rearrange array by moving element at the position like $position
	# ---
	# Operation: deactivate | activate
	# ---
	} elsif ($operation eq "de_activate") {
		my $id;
		my $deactivated_text = qq(<cite>!!! DEACTIVATED !!!<\/cite>);

		if ($blocks[$position] =~ m/kSWU:DEACTIVATED/s){											# ACTIVATE (has been deactivated before)
			$blocks[$position] =~ s/<o--kSWU:DEACTIVATED--o>$deactivated_text//s;					# remove first DEACTIVATED block
			$blocks[$position] =~ s/<o--\/kSWU:DEACTIVATED--o>//s;									# remove last DEACTIVATED block
		} else {																					# DEACTIVATE (block was active)
			if ($blocks[$position] =~ m/<o--kSWU:(\d{3}):BLOCK--o>/s){								# find the block number
				$id = $1;
			}
			$blocks[$position] =~ s/(<o--kSWU:$id:BLOCK--o>)/$1<o--kSWU:DEACTIVATED--o>$deactivated_text/sg;
			$blocks[$position] =~ s/(<o--\/kSWU:$id--o>)/<o--\/kSWU:DEACTIVATED--o>$1/sg;
		}
	# ---
	# Operation: duplicate
	# ---
	} elsif ($operation eq "duplicate") {
		my $duplicatedBlock;
		my @kSWU_tags;
		my $old_id;
		my $new_id;
		my $old_blockId = undef;
		my $new_blockId = undef;

		$duplicatedBlock = $blocks[$position];														# get the content of the block we want to duplicate
		$duplicatedBlock =~ s/(<o--kSWU)/���$1/sg;													# we want to split on each "start-kSWU-tag" so we add '���' to split on
		@kSWU_tags = split('���', $duplicatedBlock);
		$duplicatedBlock = "";																		# reset the content of the duplicated block

		foreach (@kSWU_tags){
			if ($_ =~ m/<o--kSWU:(\d{3})/s){
				$old_id = $1;
				$new_id = &getNewkSWUId($old_id, @kSWUIDs);											# procedure returns a new block id

				if (!$old_blockId){																	# the first ID will be the Block ID
					$old_blockId = $old_id;
					$new_blockId = $new_id;
				}

				$_ =~ s/(kSWU:)$old_id/$1$new_id/sg;												# substitute the old id with the new id
				$_ =~ s/(kSWU:)$old_blockId/$1$new_blockId/sg;										# the last entry in the @kSWU_tags will contain the end block tag as well, so has to subsitute this number
			}
			$duplicatedBlock .= $_;																	# add each tag to the end of the duplicated block
		}
		unshift(@blocks, $duplicatedBlock);															# add the new duplicated block to the beginning of the blocks stack/array
	# ---
	# Operation: delete
	# ---
	} elsif ($operation eq "delete") {
		$blocks[$position] = "";
	}

	# ----------------------------------------------------------
	# Write changes back to file
	# ----------------------------------------------------------
	open(FILE, ">$file") || &kError("kSWU_blockOperation", "failed to open '$file'");
	print FILE $pre_blockarea . "\n";
	foreach (@blocks){
		print FILE $_;
	}
	print FILE $post_blockarea;
	close (FILE);

	&kJumpTo($scriptURL."?mode=".$mode."&op=display&file=".$file."&base=".$base);

	# ----------------------------------------------------------
	# Sub procedure inside kSWU_blockOperation, used to find
	# a new unique kSWU number.
	# ----------------------------------------------------------
	sub getNewkSWUId{
		my ($old_number,@numberlist) = @_;

		my $new_number = $old_number;
		my $i = 0;

		WHILE: while (1){																			# go through this loop until we have a unique number
			if ($i == 1000){																		# just a check in case we can't find a new unique number
				&kError("kSWU_blockOperation.getNewBlockId", "I give up finding a unique number...");
			}
			$i++;
			$new_number++;
			
			if ($new_number > 999){																	# maximum number is 999
				$new_number = 1;
			}

			foreach (@numberlist){
				if ($new_number == $_){																# if we have this number from before
					next WHILE;																		# go to the start of the WHILE loop again
				}
			}
			last WHILE;																				# we found a new number, jump out of the while loop
		}

		if (length($new_number)==1){																# we create a three digit number, ex: 1 -> 001, 10 -> 010 etc
			$new_number = "00" . $new_number;
		} elsif (length($new_number)==2){
			$new_number = "0" . $new_number;
		}

		push (@kSWUIDs,$new_number);																# add the new unique ID to the list of all kSWU IDs

		return $new_number;
	}
}
